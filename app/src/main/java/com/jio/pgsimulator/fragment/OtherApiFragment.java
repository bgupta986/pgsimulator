package com.jio.pgsimulator.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatSpinner;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Toast;

import com.jio.pgsimulator.R;
import com.jio.pgsimulator.activity.HomeActivity;
import com.jio.pgsimulator.constant.OtherAPIArray;
import com.jio.pgsimulator.constant.RequestTypeArray;
import com.jio.pgsimulator.constant.UrlConstants;
import com.jio.pgsimulator.config.ApiConfiguration;
import com.jio.pgsimulator.model.TranPayloadData;
import com.jio.pgsimulator.model.TranRefNoArray;
import com.jio.pgsimulator.model.TranRequestProcessing;
import com.jio.pgsimulator.model.TranRequest;
import com.jio.pgsimulator.model.OtherApiRequest;
import com.jio.pgsimulator.model.OtherApiPayloadData;
import com.jio.pgsimulator.model.RequestHeader;
import com.jio.pgsimulator.service.ProgressDialogMananger;
import com.jio.pgsimulator.service.RequestHandler;
import com.jio.pgsimulator.service.ServiceManager;
import com.jio.pgsimulator.util.CommonUtil;
import com.jio.pgsimulator.util.DateUtil;
import com.jio.pgsimulator.util.EncryptionUtil;
import com.jio.pgsimulator.util.PreferenceManager;
import com.jio.pgsimulator.util.XmlUtils;

import java.util.UUID;

/**
 * Class handle CheckPaymentStatus API, StatusQuery API
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnOtherApiFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link OtherApiFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class OtherApiFragment extends Fragment {

    public static final String TAG = OtherApiFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private OnOtherApiFragmentInteractionListener mListener;

    private AppCompatSpinner apiSpinner;
    private TextInputEditText timeStampEditText, tranRefNoEditText, clientIdEditText, merchantIdText,
            startDateTimeEditText, endDateTimeEditText;

    private Button submitButton;

    private String apiUrl, requestParams, requestType;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment OtherApiFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static OtherApiFragment newInstance(String param1, String param2) {
        OtherApiFragment fragment = new OtherApiFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_other_api, container, false);
        init(view);
        return view;
    }

    @Override
    public void onStop() {
        super.onStop();
        ProgressDialogMananger.hideProgressDialg();
    }

    /**
     * This method is used to initialize layout component
     */
    private void init(View view) {
        apiSpinner = (AppCompatSpinner) view.findViewById(R.id.api_spinner);

        timeStampEditText = (TextInputEditText) view.findViewById(R.id.timestamp_txt);
        tranRefNoEditText = (TextInputEditText) view.findViewById(R.id.tran_ref_edit_text);
        merchantIdText = (TextInputEditText) view.findViewById(R.id.merchant_id_txt);
        clientIdEditText = (TextInputEditText) view.findViewById(R.id.oth_client_id_edit_text);
        startDateTimeEditText = (TextInputEditText) view.findViewById(R.id.start_date_time_edit_text);
        endDateTimeEditText = (TextInputEditText) view.findViewById(R.id.end_date_time_edit_text);
        submitButton = (Button) view.findViewById(R.id.submit_btn);

        setConfigValues();

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleSubmit();
            }
        });

        apiSpinner.setAdapter(new ArrayAdapter<>(getActivity(), R.layout.support_simple_spinner_dropdown_item, OtherAPIArray.names()));
        apiSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.d(TAG, "Position :" + position);
                Log.d(TAG, "id :" + id);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    /**
     * This method prepopulate default values to view
     */
    private void setConfigValues() {
        ApiConfiguration apiConfig = PreferenceManager.getConfigPreference(getActivity());
        timeStampEditText.setText(DateUtil.getCurrentDate());
        if (null != apiConfig) {
            merchantIdText.setText(apiConfig.getMERCHANT_ID());
            clientIdEditText.setText(apiConfig.getCLIENT_ID());
        }
    }

    /**
     * The method generate the request params for CheckPaymentStatus API
     *
     * @return request param in tilled format
     */
    private String getCheckPaymentStatusRequest() {
        ApiConfiguration apiConfig = PreferenceManager.getConfigPreference(getActivity());
        String request = null;
        requestType = ((HomeActivity) getActivity()).getRequestType();
        String apiName = OtherAPIArray.CHECK_PAYMENT_STATUS.name().replaceAll("_", "");
        if (apiConfig != null && apiConfig.getVERSION().equals("1.0")) {
            int intRequestType = 1;
            if (requestType.equalsIgnoreCase(RequestTypeArray.JSON.name())) {
                intRequestType = 2;
            }
            String requestStr = apiName + "~"
                    + intRequestType + "~"
                    + UUID.randomUUID().toString() + "~"
                    + "NA" + "~"
                    + "NA" + "~"
                    + merchantIdText.getText().toString().trim() + "~"
                    + tranRefNoEditText.getText().toString().trim();
            //CHECKPAYMENTSTATUS~1~1ab85699-25ff-4930-bda9-066f9c482509~NA~NA~100001000013622~456655566235~4206648211
            EncryptionUtil eu = new EncryptionUtil();
            String encryptedString = eu.hmacDigest(requestStr, PreferenceManager.getConfigPreference(getActivity()).getCHECKSUM_SEED(), EncryptionUtil.ALGORITHM);
            request = requestStr + "~" + encryptedString;
        } else {
            //TODO:: Code for API Version 2.0
            String tranRefNo = tranRefNoEditText.getText().toString().trim();
            RequestHeader requestHeader = new RequestHeader(apiName, timeStampEditText.getText().toString().trim());
            TranRequestProcessing tranRequestProcessing = null;
            if (tranRefNo.contains(",")) {
                String tranRefNoArr[] = tranRefNo.split(",");
                TranRefNoArray tranRefNoArray = new TranRefNoArray(tranRefNoArr);
                TranPayloadData tranPayloadData = new TranPayloadData(merchantIdText.getText().toString().trim(), tranRefNoArray);
                tranRequestProcessing = new TranRequestProcessing(requestHeader, tranPayloadData, generateChecksum(apiName));

//                String tranRefNoArr[] = tranRefNo.split(",");
//                TranRefNoArray tranRefNoArray = new TranRefNoArray(Arrays.asList(tranRefNoArr));
//                TranPayloadData tranPayloadData = new TranPayloadData(merchantIdText.getText().toString().trim(), tranRefNoArray);
//                tranRequestProcessing = new TranRequestProcessing(requestHeader, tranPayloadData);
                Log.d(TAG, "--More than 1 tranRefNo. contains ,(Comma)--");
            } else if (tranRefNo.contains("|")) {
                String tranRefNoArr[] = tranRefNo.split("\\|");
                TranRefNoArray tranRefNoArray = new TranRefNoArray(tranRefNoArr);
                TranPayloadData tranPayloadData = new TranPayloadData(merchantIdText.getText().toString().trim(), tranRefNoArray);
                tranRequestProcessing = new TranRequestProcessing(requestHeader, tranPayloadData, generateChecksum(apiName));
                Log.d(TAG, "--More than 1 tranRefNo. contains |(Pipe)--");
            } else {
                String strTranRefNoArr[] = {tranRefNo};
                TranRefNoArray tranRefNoArray = new TranRefNoArray(strTranRefNoArr);
                TranPayloadData tranPayloadData = new TranPayloadData(merchantIdText.getText().toString().trim(), tranRefNoArray);
                tranRequestProcessing = new TranRequestProcessing(requestHeader, tranPayloadData, generateChecksum(apiName));
                Log.d(TAG, "--Only one tranRefNo.--");
            }

            if (requestType.equalsIgnoreCase(RequestTypeArray.JSON.name())) {
                TranRequest tranRequest = new TranRequest(tranRequestProcessing);
                request = CommonUtil.getJsonString(tranRequest);
            } else if (requestType.equalsIgnoreCase(RequestTypeArray.XML.name())) {
                request = XmlUtils.generateXmlParams(tranRequestProcessing);
            }
        }
        return request;
    }

    private String getRequestStatusRequest() {
        ApiConfiguration apiConfig = PreferenceManager.getConfigPreference(getActivity());
        String request = null;
        requestType = ((HomeActivity) getActivity()).getRequestType();
        String apiName = OtherAPIArray.GET_REQUEST_STATUS.name().replaceAll("_", "");
        if (apiConfig != null && apiConfig.getVERSION().equals("1.0")) {
            int intRequestType = 1;
            if (requestType.equalsIgnoreCase(RequestTypeArray.JSON.name())) {
                intRequestType = 2;
            }
            String requestStr = apiName + "~"
                    + intRequestType + "~"
                    + UUID.randomUUID().toString() + "~"
                    + "NA" + "~"
                    + "NA" + "~"
                    + merchantIdText.getText().toString().trim() + "~"
                    + tranRefNoEditText.getText().toString().trim();
            //GETREQUESTSTATUS~1~033cc6d9-b1b4-425b-872a-b890847b2593~NA~NA~100001000013519~525312007865|525312007872~2961942474
            EncryptionUtil eu = new EncryptionUtil();
            String encryptedString = eu.hmacDigest(requestStr, PreferenceManager.getConfigPreference(getActivity()).getCHECKSUM_SEED(), EncryptionUtil.ALGORITHM);
            request = requestStr + "~" + encryptedString;
        } else {
            //TODO:: Code for API Version 2.0
            RequestHeader requestHeader = new RequestHeader(apiName, timeStampEditText.getText().toString().trim());
            TranPayloadData tranPayloadData = new TranPayloadData(merchantIdText.getText().toString().trim());
            TranRequestProcessing tranRequestProcessing = new TranRequestProcessing(requestHeader, tranPayloadData, generateChecksum(apiName));

            if (requestType.equalsIgnoreCase(RequestTypeArray.JSON.name())) {
                TranRequest tranRequest = new TranRequest(tranRequestProcessing);
                request = CommonUtil.getJsonString(tranRequest);
            } else if (requestType.equalsIgnoreCase(RequestTypeArray.XML.name())) {
                request = XmlUtils.generateXmlParams(tranRequestProcessing);
            }
        }
        return request;
    }

    private String getMDRRequest() {
        ApiConfiguration apiConfig = PreferenceManager.getConfigPreference(getActivity());
        String request = null;
        requestType = ((HomeActivity) getActivity()).getRequestType();
        String apiName = OtherAPIArray.GET_MDR.name().replaceAll("_", "");
        if (apiConfig != null && apiConfig.getVERSION().equals("1.0")) {
            int intRequestType = 1;
            if (requestType.equalsIgnoreCase(RequestTypeArray.JSON.name())) {
                intRequestType = 2;
            }
            String requestStr = apiName + "~"
                    + intRequestType + "~"
                    + UUID.randomUUID().toString() + "~"
                    + "NA" + "~"
                    + "NA" + "~"
                    + merchantIdText.getText().toString().trim() + "~"
                    + tranRefNoEditText.getText().toString().trim();
            //GETMDR~1~5bc51553-5170-4e52-9367-7fd5af48f9a8~NA~NA~100001000013266~528814010692|528814010697|528816010714~3883212642
            EncryptionUtil eu = new EncryptionUtil();
            String encryptedString = eu.hmacDigest(requestStr, PreferenceManager.getConfigPreference(getActivity()).getCHECKSUM_SEED(), EncryptionUtil.ALGORITHM);
            request = requestStr + "~" + encryptedString;
        } else {
            //TODO:: Code for API Version 2.0
            RequestHeader requestHeader = new RequestHeader(apiName, timeStampEditText.getText().toString().trim());
            TranPayloadData tranPayloadData = new TranPayloadData(merchantIdText.getText().toString().trim());
            TranRequestProcessing tranRequestProcessing = new TranRequestProcessing(requestHeader, tranPayloadData, generateChecksum(apiName));

            if (requestType.equalsIgnoreCase(RequestTypeArray.JSON.name())) {
                TranRequest tranRequest = new TranRequest(tranRequestProcessing);
                request = CommonUtil.getJsonString(tranRequest);
            } else if (requestType.equalsIgnoreCase(RequestTypeArray.XML.name())) {
                request = XmlUtils.generateXmlParams(tranRequestProcessing);
            }
        }
        return request;
    }

    private String getTransactionDetailsRequest() {
        ApiConfiguration apiConfig = PreferenceManager.getConfigPreference(getActivity());
        String request = null;
        requestType = ((HomeActivity) getActivity()).getRequestType();
        String apiName = OtherAPIArray.GET_TRANSACTION_DETAILS.name().replaceAll("_", "");
        if (apiConfig != null && apiConfig.getVERSION().equals("1.0")) {
            int intRequestType = 1;
            if (requestType.equalsIgnoreCase(RequestTypeArray.JSON.name())) {
                intRequestType = 2;
            }
            String requestStr = apiName + "~"
                    + intRequestType + "~"
                    + UUID.randomUUID().toString() + "~"
                    + "NA" + "~"
                    + "NA" + "~"
                    + merchantIdText.getText().toString().trim() + "~"
                    + tranRefNoEditText.getText().toString().trim();
            //GETTRANSACTIONDETAILS~1~d745050c-af0a-4d15-8147-f2fcae00c1cf~NA~NA~100001000013519~525312007865|525312007872~834351033
            EncryptionUtil eu = new EncryptionUtil();
            String encryptedString = eu.hmacDigest(requestStr, PreferenceManager.getConfigPreference(getActivity()).getCHECKSUM_SEED(), EncryptionUtil.ALGORITHM);
            request = requestStr + "~" + encryptedString;
        } else {
            //TODO:: Code for API Version 2.0
            String tranRefNo = tranRefNoEditText.getText().toString().trim();
            RequestHeader requestHeader = new RequestHeader(apiName, timeStampEditText.getText().toString().trim());
            TranRequestProcessing tranRequestProcessing;
            if (tranRefNo.contains(",")) {
                String tranRefNoArr[] = tranRefNo.split(",");
                TranRefNoArray tranRefNoArray = new TranRefNoArray(tranRefNoArr);
                TranPayloadData tranPayloadData = new TranPayloadData(merchantIdText.getText().toString().trim(), tranRefNoArray);
                tranRequestProcessing = new TranRequestProcessing(requestHeader, tranPayloadData, generateChecksum(apiName));
                Log.d(TAG, "--More than 1 tranRefNo. contains ,(Comma)--");
            } else if (tranRefNo.contains("|")) {
                String tranRefNoArr[] = tranRefNo.split("\\|");
                TranRefNoArray tranRefNoArray = new TranRefNoArray(tranRefNoArr);
                TranPayloadData tranPayloadData = new TranPayloadData(merchantIdText.getText().toString().trim(), tranRefNoArray);
                tranRequestProcessing = new TranRequestProcessing(requestHeader, tranPayloadData, generateChecksum(apiName));
                Log.d(TAG, "--More than 1 tranRefNo. contains |(Pipe)--");
            } else {
                String strTranRefNoArr[] = {tranRefNo};
                TranRefNoArray tranRefNoArray = new TranRefNoArray(strTranRefNoArr);
                TranPayloadData tranPayloadData = new TranPayloadData(merchantIdText.getText().toString().trim(), tranRefNoArray);
                tranRequestProcessing = new TranRequestProcessing(requestHeader, tranPayloadData, generateChecksum(apiName));
                Log.d(TAG, "--Only one tranRefNo.--");
            }

            if (requestType.equalsIgnoreCase(RequestTypeArray.JSON.name())) {
                TranRequest tranRequest = new TranRequest(tranRequestProcessing);
                request = CommonUtil.getJsonString(tranRequest);
            } else if (requestType.equalsIgnoreCase(RequestTypeArray.XML.name())) {
                request = XmlUtils.generateXmlParams(tranRequestProcessing);
            }
        }
        return request;
    }

    private String getFetchTransactionPeriodRequest() {
        ApiConfiguration apiConfig = PreferenceManager.getConfigPreference(getActivity());
        String request = null;
        requestType = ((HomeActivity) getActivity()).getRequestType();
        String apiName = OtherAPIArray.FETCH_TRANSACTION_PERIOD.name().replaceAll("_", "");
        if (apiConfig != null && apiConfig.getVERSION().equals("1.0")) {

            int intRequestType = 1;
            if (requestType.equalsIgnoreCase(RequestTypeArray.JSON.name())) {
                intRequestType = 2;
            }

            String requestStr = apiName + "~"
                    + intRequestType + "~"
                    + UUID.randomUUID().toString() + "~"
                    + startDateTimeEditText.getText().toString().trim() + "~"
                    + endDateTimeEditText.getText().toString().trim() + "~"
                    + merchantIdText.getText().toString().trim() + "~"
                    + "NA";
            //FETCHTRANSACTIONPERIOD~1~efd497df-30d4-4c2d-b404-5e987dc52289~2015100617000000~2015100617030000~100001000013692~NA~175674187
            //apiname~mode~requestid~Start DateTime~End DateTime~Merchant ID~Transaction ID~Checksum

            EncryptionUtil eu = new EncryptionUtil();
            String encryptedString = eu.hmacDigest(requestStr, PreferenceManager.getConfigPreference(getActivity()).getCHECKSUM_SEED(), EncryptionUtil.ALGORITHM);
            request = requestStr + "~" + encryptedString;
        } else {
            //TODO:: Code for API Version 2.0
            RequestHeader requestHeader = new RequestHeader(apiName, timeStampEditText.getText().toString().trim());
            TranPayloadData tranPayloadData = new TranPayloadData(merchantIdText.getText().toString().trim(),
                    startDateTimeEditText.getText().toString().trim(), endDateTimeEditText.getText().toString().trim());
            TranRequestProcessing tranRequestProcessing = new TranRequestProcessing(requestHeader, tranPayloadData, generateChecksum(apiName));
            if (requestType.equalsIgnoreCase(RequestTypeArray.JSON.name())) {
                TranRequest tranRequest = new TranRequest(tranRequestProcessing);
                request = CommonUtil.getJsonString(tranRequest);
            } else if (requestType.equalsIgnoreCase(RequestTypeArray.XML.name())) {
                request = XmlUtils.generateXmlParams(tranRequestProcessing);
            }
        }
        return request;
    }

    private String getTodayDataRequest() {
        ApiConfiguration apiConfig = PreferenceManager.getConfigPreference(getActivity());
        String request = null;
        requestType = ((HomeActivity) getActivity()).getRequestType();
        String apiName = OtherAPIArray.GET_TODAYS_DATA.name().replaceAll("_", "");
        if (apiConfig != null && apiConfig.getVERSION().equals("1.0")) {
            int intRequestType = 1;
            if (requestType.equalsIgnoreCase(RequestTypeArray.JSON.name())) {
                intRequestType = 2;
            }
            String requestStr = apiName + "~"
                    + intRequestType + "~"
                    + UUID.randomUUID().toString() + "~"
                    + "NA" + "~"
                    + "NA" + "~"
                    + merchantIdText.getText().toString().trim() + "~"
                    + "NA";
            //GETTODAYSDATA~1~e49f0dfe-157e-4a48-bb1c-09db90d81252~NA~NA~100001000013692~NA(TRAN_REF_NO.)~3843023970
            EncryptionUtil eu = new EncryptionUtil();
            String encryptedString = eu.hmacDigest(requestStr, PreferenceManager.getConfigPreference(getActivity()).getCHECKSUM_SEED(), EncryptionUtil.ALGORITHM);
            request = requestStr + "~" + encryptedString;
        } else {
            //TODO:: Code for API Version 2.0
            RequestHeader requestHeader = new RequestHeader(apiName, timeStampEditText.getText().toString().trim());
            TranPayloadData tranPayloadData = new TranPayloadData(merchantIdText.getText().toString().trim());
            TranRequestProcessing tranRequestProcessing = new TranRequestProcessing(requestHeader, tranPayloadData, generateChecksum(apiName));
            if (requestType.equalsIgnoreCase(RequestTypeArray.JSON.name())) {
                TranRequest tranRequest = new TranRequest(tranRequestProcessing);
                request = CommonUtil.getJsonString(tranRequest);
            } else if (requestType.equalsIgnoreCase(RequestTypeArray.XML.name())) {
                request = XmlUtils.generateXmlParams(tranRequestProcessing);
            }
        }
        return request;
    }

    private String getStatusQueryRequest() {
        String apiName = OtherAPIArray.STATUS_QUERY.name().replaceAll("_", "");
        String request = null;
        if (requestType.equalsIgnoreCase(RequestTypeArray.JSON.name())) {
            request = CommonUtil.getJsonString(getRequestXMLObj(otherApiPayloadData(), apiName));
        } else if (requestType.equalsIgnoreCase(RequestTypeArray.XML.name())) {
            ApiConfiguration apiConfig = PreferenceManager.getConfigPreference(getActivity());
            request = XmlUtils.generateXmlParams(getRequestXMLObj(otherApiPayloadData(), apiName), apiConfig.getVERSION());
        }
        return request;
    }

    /**
     * Method is handling business logic and setting the request params to different API's and calls it from this method
     */
    private void handleSubmit() {
        requestType = ((HomeActivity) getActivity()).getRequestType();
        String apiName = null;
        if (OtherAPIArray.CHECK_PAYMENT_STATUS.ordinal() == apiSpinner.getSelectedItemPosition()) {
            apiName = OtherAPIArray.CHECK_PAYMENT_STATUS.name().replaceAll("_", "");
            requestType = "";
            apiUrl = UrlConstants.TRANSACTION_INQUIRY_URL;
            requestParams = getCheckPaymentStatusRequest();
        } else if (OtherAPIArray.GET_REQUEST_STATUS.ordinal() == apiSpinner.getSelectedItemPosition()) {
            apiName = OtherAPIArray.GET_REQUEST_STATUS.name().replaceAll("_", "");
            requestType = "";
            apiUrl = UrlConstants.TRANSACTION_INQUIRY_URL;
            requestParams = getRequestStatusRequest();
        } else if (OtherAPIArray.GET_MDR.ordinal() == apiSpinner.getSelectedItemPosition()) {
            apiName = OtherAPIArray.GET_MDR.name().replaceAll("_", "");
            requestType = "";
            apiUrl = UrlConstants.TRANSACTION_INQUIRY_URL;
            requestParams = getMDRRequest();
        } else if (OtherAPIArray.GET_TRANSACTION_DETAILS.ordinal() == apiSpinner.getSelectedItemPosition()) {
            apiName = OtherAPIArray.GET_TRANSACTION_DETAILS.name().replaceAll("_", "");
            requestType = "";
            apiUrl = UrlConstants.TRANSACTION_INQUIRY_URL;
            //requestParams = getTransactionDetailsRequest();
        } else if (OtherAPIArray.FETCH_TRANSACTION_PERIOD.ordinal() == apiSpinner.getSelectedItemPosition()) {
            apiName = OtherAPIArray.FETCH_TRANSACTION_PERIOD.name().replaceAll("_", "");
            requestType = "";
            apiUrl = UrlConstants.TRANSACTION_INQUIRY_URL;
            requestParams = getFetchTransactionPeriodRequest();
        } else if (OtherAPIArray.GET_TODAYS_DATA.ordinal() == apiSpinner.getSelectedItemPosition()) {
            apiName = OtherAPIArray.GET_TODAYS_DATA.name().replaceAll("_", "");
            requestType = "";
            apiUrl = UrlConstants.TRANSACTION_INQUIRY_URL;
            requestParams = getTodayDataRequest();
        } else if (OtherAPIArray.STATUS_QUERY.ordinal() == apiSpinner.getSelectedItemPosition()) {
            apiName = OtherAPIArray.STATUS_QUERY.name().replaceAll("_", "");
            apiUrl = UrlConstants.STATUS_QUERY_URL;
            requestParams = getStatusQueryRequest();
        }

        Log.d(TAG, "API NAME = " + apiName);
        Log.d(TAG, "API URL = " + apiUrl);
        Log.d(TAG, "API Request Type = " + requestType);
        Log.d(TAG, "API Request Params = " + requestParams);

        ServiceManager queryStatusService = new ServiceManager(getActivity(), apiUrl, requestParams, ServiceManager.HttpMethod.POST, requestType);
        queryStatusService.handleRequest(new RequestHandler() {
            @Override
            public void onStart() {
                Log.d(TAG, "onStart");
                ProgressDialogMananger.showProgress(getActivity());
            }

            @Override
            public void onStop() {
                Log.d(TAG, "onStop");
                ProgressDialogMananger.hideProgressDialg();
            }

            @Override
            public void onSuccess(int responseCode, String response) {
                Log.d(TAG, "onSuccess");
                Log.d(TAG, "responseCode :" + responseCode + " response" + response);
                ProgressDialogMananger.hideProgressDialg();
                if (isAdded()) {
                }
                mListener.onOtherApiFragmentInteraction(requestParams, response, apiUrl);
            }

            @Override
            public void OnError(int errorCode, String errorMessage) {
                Log.d(TAG, "OnError");
                Log.d(TAG, "ErrorCode :" + errorCode + " errorMessage: " + errorMessage);
                ProgressDialogMananger.hideProgressDialg();
                if (isAdded()) {
                    //Toast.makeText(getActivity(), "ErrorMsg: " + errorMessage, Toast.LENGTH_LONG).show();
                }
                mListener.onOtherApiFragmentInteraction(requestParams, errorMessage, apiUrl);
            }

            @Override
            public void OnNetworkConnectionError(int errorCode, String response) {
                if (isAdded()) {
                    //Toast.makeText(getActivity(), response, Toast.LENGTH_LONG).show();
                }
            }
        });
        queryStatusService.execute();
//        }
    }

    /**
     * Method gets getRequestJSONObj for StatusQuery API in json
     * @param otherApiPayloadRequest
     * @param checksumData
     * @param apiName
     * @return
     */
    /*private OtherApiRequest getRequestJSONObj(OtherApiPayloadData otherApiPayloadRequest, String checksumData, String apiName){
        RequestHeader requestHeader = new RequestHeader(apiName, "1");
        EncryptionUtil eu =  new EncryptionUtil();
        String encryptedString = eu.hmacDigest(checksumData, EncryptionUtil.CHECKSUM_SEED, EncryptionUtil.ALGORITHM);
        OtherApiRequest otherApi = new OtherApiRequest(requestHeader, encryptedString, otherApiPayloadRequest);
        OtherApiRequest otherApiRequest = new OtherApiRequest(otherApi);
        return otherApiRequest;
    }*/

    /**
     * Method gets getRequestJSONObj for StatusQuery API in XML
     *
     * @param otherApiPayloadRequest
     * @param apiName
     * @return
     */
    private OtherApiRequest getRequestXMLObj(OtherApiPayloadData otherApiPayloadRequest, String apiName) {
        ApiConfiguration apiConfig = PreferenceManager.getConfigPreference(getActivity());
        OtherApiRequest otherApiRequest = null;
        if (apiConfig != null) {
            RequestHeader requestHeader = new RequestHeader(apiConfig.getVERSION(), apiName, "");
            otherApiRequest = new OtherApiRequest(requestHeader, otherApiPayloadRequest, generateStatusQueryChecksum());
        }
        return otherApiRequest;
    }

    /**
     * MEhtod get payload data for StatusQuery API
     *
     * @return
     */
    private OtherApiPayloadData otherApiPayloadData() {
        return new OtherApiPayloadData(merchantIdText.getText().toString().trim(), tranRefNoEditText.getText().toString().trim(), clientIdEditText.getText().toString().trim());
    }

    /**
     * Method creates the checksum string for CheckPaymentStatus API, StatusQuery API
     *
     * @return
     */
    private String generateStatusQueryChecksum() {
        String apiName = OtherAPIArray.STATUS_QUERY.name().replaceAll("_", "");
        String checksumData = clientIdEditText.getText().toString().trim() + "|" +
                merchantIdText.getText().toString().trim() + "|" +
                apiName + "|" + tranRefNoEditText.getText().toString().trim();

        EncryptionUtil eu = new EncryptionUtil();
        String encryptedString = eu.hmacDigest(checksumData, PreferenceManager.getConfigPreference(getActivity()).getCHECKSUM_SEED(), EncryptionUtil.ALGORITHM);

        Log.d(TAG, "checksumData = " + checksumData);
        return encryptedString;
    }

    private String generateChecksum(String apiName) {
        String checksumData = apiName + "|" + timeStampEditText.getText().toString().trim() + "|" + merchantIdText.getText().toString().trim();
        EncryptionUtil eu = new EncryptionUtil();
        String encryptedChecksum = eu.hmacDigest(checksumData, PreferenceManager.getConfigPreference(getActivity()).getCHECKSUM_SEED(), EncryptionUtil.ALGORITHM);
        Log.d(TAG, "checksumData = " + checksumData);
        Log.d(TAG, "encryptedChecksum = " + encryptedChecksum);

        return encryptedChecksum;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnOtherApiFragmentInteractionListener) {
            mListener = (OnOtherApiFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnOtherApiFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnOtherApiFragmentInteractionListener {
        // TODO: Update argument type and name
        void onOtherApiFragmentInteraction(String request, String response, String apiUrl);
    }
}
