package com.jio.pgsimulator.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.jio.pgsimulator.R;
import com.jio.pgsimulator.activity.HomeActivity;
import com.jio.pgsimulator.constant.UrlConstants;
import com.jio.pgsimulator.config.ApiConfiguration;
import com.jio.pgsimulator.model.RefundRequest;
import com.jio.pgsimulator.model.RefundTransaction;
import com.jio.pgsimulator.service.ProgressDialogMananger;
import com.jio.pgsimulator.service.RequestHandler;
import com.jio.pgsimulator.service.ServiceManager;
import com.jio.pgsimulator.util.CommonUtil;
import com.jio.pgsimulator.util.DateUtil;
import com.jio.pgsimulator.util.EncryptionUtil;
import com.jio.pgsimulator.util.PreferenceManager;
import com.jio.pgsimulator.util.ValidationUtil;

/**
 * Fragment class manages refund process
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnRefundFragmentInteractionListener} interface
 * to handle interaction events.
 * create an instance of this fragment.
 */
public class RefundFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private final String TAG = RefundFragment.class.getSimpleName();
    // TODO: Rename and change types of parameters

    private OnRefundFragmentInteractionListener mListener;
    private static final String CHANNEL = "WEB";
    private static final String RETURN_URL = "NA";
    private static final String REFUND_API = "REFUND";

    private View view;
    private TextInputLayout lm_client_id_input, lm_merchant_id_input, lm_timestamp_input,
            lm_transaction_ext_ref_input, lm_transaction_currency_input, lm_return_url_input;
    private TextInputEditText clientIdEditText, merchantIdEditText, channel, timeStampEditText, transactionExtRefEditText,
            transactionCurrencyEditText, transactionAmountEditText, returnUrlEditText, originalTimeStamp, originalTxnRef;
    private Button submitButton;
    private String requestParams, apiUrl;

    public RefundFragment() {
        // Required empty public constructor
    }

    public static RefundFragment newInstance() {
        RefundFragment refundFragment = new RefundFragment();
        return refundFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /**
     * life cycle method of fragment
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_refund, container, false);
        init();
        setConfigValues();
        return view;
    }

    /**
     * \
     * The method initialize all the UI component
     */
    private void init() {

        lm_client_id_input = (TextInputLayout) view.findViewById(R.id.lm_client_id_input);
        lm_merchant_id_input = (TextInputLayout) view.findViewById(R.id.lm_merchant_id_input);
        lm_timestamp_input = (TextInputLayout) view.findViewById(R.id.lm_timestamp_input);
        lm_transaction_ext_ref_input = (TextInputLayout) view.findViewById(R.id.lm_transaction_ext_ref_input);
        lm_transaction_currency_input = (TextInputLayout) view.findViewById(R.id.lm_transaction_currency_input);
        lm_return_url_input = (TextInputLayout) view.findViewById(R.id.lm_return_url_input);


        clientIdEditText = (TextInputEditText) view.findViewById(R.id.lm_client_id_txt);
        merchantIdEditText = (TextInputEditText) view.findViewById(R.id.lm_merchant_id_edit_txt);
        channel = (TextInputEditText) view.findViewById(R.id.lm_channel_edit_txt);
        transactionExtRefEditText = (TextInputEditText) view.findViewById(R.id.lm_transaction_ext_ref_edit_txt);
        transactionCurrencyEditText = (TextInputEditText) view.findViewById(R.id.lm_transaction_currency_edit_txt);
        transactionAmountEditText = (TextInputEditText) view.findViewById(R.id.lm_transaction_amount_edit_txt);
        returnUrlEditText = (TextInputEditText) view.findViewById(R.id.lm_return_url_edit_txt);
        originalTimeStamp = (TextInputEditText) view.findViewById(R.id.lm_original_timestamp_edit_txt);
        originalTxnRef = (TextInputEditText) view.findViewById(R.id.lm_original_ref_edit_txt);
        timeStampEditText = (TextInputEditText) view.findViewById(R.id.lm_time_stamp_edit_txt);

        submitButton = (Button) view.findViewById(R.id.submit_btn);

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonUtil.hideKeyboard(getActivity());
                if (validateData()) {
                    handleSubmit();
                }
//                handleSubmit();
            }
        });


    }

    /**
     * Method is handling bussiness logic and setting the request params to different REFUND_API's and calls it from this method
     */

    private void handleSubmit() {
        Log.d(TAG, "requestType: " + ((HomeActivity) getActivity()).getRequestType());
        if (validateData()) {

            String requestType = ((HomeActivity) getActivity()).getRequestType();
            /**
             * Handling the request
             */

            apiUrl = UrlConstants.REFUND_URL;
            RefundTransaction refundTransaction = new RefundTransaction(transactionAmountEditText.getText().toString().trim(), transactionCurrencyEditText.getText().toString().trim(),
                    transactionExtRefEditText.getText().toString().trim(), REFUND_API, timeStampEditText.getText().toString().trim());
            RefundRequest refundRequest = new RefundRequest(merchantIdEditText.getText().toString().trim(),
                    clientIdEditText.getText().toString().trim(),
                    CHANNEL,
                    RETURN_URL,
                    "",
                    generateCheckSum(),
                    getRefundInfo(),
                    refundTransaction);

            requestParams = CommonUtil.getJsonString(refundRequest);


            Log.d(TAG, "requestParams :" + requestParams);
            ServiceManager refundService = new ServiceManager(getActivity(), apiUrl, requestParams,
                    ServiceManager.HttpMethod.POST, requestType);
            refundService.handleRequest(new RequestHandler() {
                @Override
                public void onStart() {
                    Log.d(TAG, "onStart");
                    ProgressDialogMananger.showProgress(getActivity());
                }

                @Override
                public void onStop() {
                    Log.d(TAG, "onStop");
                    ProgressDialogMananger.hideProgressDialg();
                }

                @Override
                public void onSuccess(int responseCode, String response) {
                    Log.d(TAG, "onSuccess");
                    Log.d(TAG, "responseCode :" + responseCode + " response" + response);
                    ProgressDialogMananger.hideProgressDialg();
                    if (isAdded()) {
//                        Toast.makeText(getActivity(), "Response: " + response, Toast.LENGTH_LONG).show();
                        mListener.onRefundFragmentInteraction(requestParams, response, apiUrl);
                    }

                }

                @Override
                public void OnError(int errorCode, String errorMessage) {
                    Log.d(TAG, "OnError");
                    Log.d(TAG, "ErrorCode :" + errorCode + " errorMessage: " + errorMessage);
                    ProgressDialogMananger.hideProgressDialg();
                    if (isAdded()) {
//                        Toast.makeText(getActivity(), "ErrorMsg: " + errorMessage, Toast.LENGTH_LONG).show();
                        mListener.onRefundFragmentInteraction(requestParams, errorMessage, apiUrl);
                    }
                }

                @Override
                public void OnNetworkConnectionError(int errorCode, String response) {
                    if (isAdded()) {
                        Toast.makeText(getActivity(), response, Toast.LENGTH_LONG).show();
                    }
                }
            });
            refundService.execute();
        }
    }


    /**
     * The method returns Original Refund info
     *
     * @return
     */
    private String getRefundInfo() {
        return originalTxnRef.getText().toString().trim() + "|" + originalTimeStamp.getText().toString().trim() + "|NA";
    }

    /**
     * The method checks the data validatation
     *
     * @return true when validation is successfull else return false
     */
    private Boolean validateData() {
        Log.d(TAG, "validateData ");

        clearErrorLayout();

        if (null != lm_client_id_input && lm_client_id_input.getVisibility() == View.VISIBLE && clientIdEditText.getText().toString().trim().length() <= 0) {
            ValidationUtil.setErrorLayout(lm_client_id_input, "Invalid Client ID!", clientIdEditText);
            return false;
        }
        if (null != lm_merchant_id_input && lm_merchant_id_input.getVisibility() == View.VISIBLE && merchantIdEditText.getText().toString().trim().length() <= 0) {
            ValidationUtil.setErrorLayout(lm_merchant_id_input, "Invalid Merchant ID!", merchantIdEditText);
            return false;
        }
        if (null != lm_timestamp_input && lm_timestamp_input.getVisibility() == View.VISIBLE && timeStampEditText.getText().toString().trim().length() <= 0) {
            ValidationUtil.setErrorLayout(lm_timestamp_input, "Invalid Timestamp!!", timeStampEditText);
            return false;
        }

        if (null != lm_transaction_ext_ref_input && lm_transaction_ext_ref_input.getVisibility() == View.VISIBLE && transactionExtRefEditText.getText().toString().trim().length() <= 0) {
            ValidationUtil.setErrorLayout(lm_transaction_ext_ref_input, "Invalid Ext. Ref!", transactionExtRefEditText);
            return false;
        }
        if (null != lm_transaction_currency_input && lm_transaction_currency_input.getVisibility() == View.VISIBLE && transactionCurrencyEditText.getText().toString().trim().length() <= 0) {
            ValidationUtil.setErrorLayout(lm_transaction_currency_input, "Invalid Transaction currency!", transactionCurrencyEditText);
            return false;
        }
        if (null != lm_return_url_input && lm_return_url_input.getVisibility() == View.VISIBLE && returnUrlEditText.getText().toString().trim().length() <= 0) {
            ValidationUtil.setErrorLayout(lm_return_url_input, "Invalid ReturnUrl!", returnUrlEditText);
            return false;
        }

        return true;
    }

    /**
     * The method clears all error message
     */
    private void clearErrorLayout() {
        Log.d(TAG, "clearErrorLayout ");

        lm_client_id_input.setErrorEnabled(false);
        lm_merchant_id_input.setErrorEnabled(false);
        lm_timestamp_input.setErrorEnabled(false);
        lm_transaction_ext_ref_input.setErrorEnabled(false);
        lm_transaction_currency_input.setErrorEnabled(false);
        lm_return_url_input.setErrorEnabled(false);

        lm_client_id_input.setError(null);
        lm_merchant_id_input.setError(null);
        lm_timestamp_input.setError(null);
        lm_transaction_ext_ref_input.setError(null);
        lm_transaction_currency_input.setError(null);
        lm_return_url_input.setError(null);

        clientIdEditText.clearFocus();
        merchantIdEditText.clearFocus();
        timeStampEditText.clearFocus();
        transactionExtRefEditText.clearFocus();
        transactionCurrencyEditText.clearFocus();
        returnUrlEditText.clearFocus();

    }

    /**
     * Lifecycle method removes the progress dialog from application
     */
    @Override
    public void onStop() {
        super.onStop();
        ProgressDialogMananger.hideProgressDialg();
    }

    /**
     * The method sets default values to UI
     */
    private void setConfigValues() {
        ApiConfiguration apiConfig = PreferenceManager.getConfigPreference(getActivity());
        timeStampEditText.setText(DateUtil.getCurrentDate());
        transactionCurrencyEditText.setText("INR");
        channel.setText("WEB");
        transactionAmountEditText.setText("1.00");
        returnUrlEditText.setText(RETURN_URL);
        if (null != apiConfig) {
            merchantIdEditText.setText(apiConfig.getMERCHANT_ID());
            clientIdEditText.setText(apiConfig.getCLIENT_ID());

        }


    }

    /**
     * The method generate the checksum string for REFUND_API call
     *
     * @return
     */
    private String generateCheckSum() {
        String checksumData = clientIdEditText.getText().toString().trim() + "|" + transactionAmountEditText.getText().toString().trim()
                + "|" + transactionExtRefEditText.getText().toString().trim() + "|" + channel.getText().toString() + "|"
                + merchantIdEditText.getText().toString().trim() + "||" + RETURN_URL + "|"
                + timeStampEditText.getText().toString().trim()
                + "|" + REFUND_API;
        Log.d(TAG, "checksumData  :" + checksumData);
        EncryptionUtil eu = new EncryptionUtil();
        String encryptedString = eu.hmacDigest(checksumData, PreferenceManager.getConfigPreference(getActivity()).getCHECKSUM_SEED(), EncryptionUtil.ALGORITHM);
        return encryptedString;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnRefundFragmentInteractionListener) {
            mListener = (OnRefundFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnRefundFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnRefundFragmentInteractionListener {
        // TODO: Update argument type and name
        void onRefundFragmentInteraction(String req, String resp, String apiUrl);
    }
}
