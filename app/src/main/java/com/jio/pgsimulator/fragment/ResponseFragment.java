package com.jio.pgsimulator.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jio.pgsimulator.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnResponseFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ResponseFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ResponseFragment extends Fragment {

    public static final String TAG = ResponseFragment.class.getSimpleName();
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String REQUEST_PARAMS = "param1";
    private static final String RESPONSE_PARAMS = "param2";
    private static final String API_URL = "api_url";
    private static final String REQUEST_TYPE = "request_type";

    // TODO: Rename and change types of parameters
    private String requestParams;
    private String responseParams;
    private String apiUrl;
    private String requestType;

    private OnResponseFragmentInteractionListener mListener;

    private TextView requestText, responseText, apiUrlText;

    public ResponseFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param requestParams Parameter 1.
     * @param responseParams Parameter 2.
     * @return A new instance of fragment ResponseFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ResponseFragment newInstance(String requestParams, String responseParams, String apiUrl, String requestType) {
        ResponseFragment fragment = new ResponseFragment();
        Bundle args = new Bundle();
        args.putString(REQUEST_PARAMS, requestParams);
        args.putString(RESPONSE_PARAMS, responseParams);
        args.putString(API_URL, apiUrl);
        args.putString(REQUEST_TYPE, requestType);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
     public void onResume() {
        super.onResume();
//        ((HomeActivity)getActivity()).hideMenu();
        Log.d(TAG, "onResume");
//        ((HomeActivity)getActivity()).setRequestTypeSpinner(requestType);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            requestParams = getArguments().getString(REQUEST_PARAMS);
            responseParams = getArguments().getString(RESPONSE_PARAMS);
            apiUrl = getArguments().getString(API_URL);
            requestType = getArguments().getString(REQUEST_TYPE);
        }
        setHasOptionsMenu(true);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            getActivity().onBackPressed();
            return true;
        }
        return false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.new_fragment_response, container, false);
        getActivity().setTitle("RESPONSE");
//        ((HomeActivity)getActivity()).toggleHome(false);
        init(view);
        return view;

    }

    private void init(View view){
        requestText = (TextView)view.findViewById(R.id.request_text);
        responseText = (TextView)view.findViewById(R.id.response_text);
        apiUrlText = (TextView)view.findViewById(R.id.api_url);

        requestText.setMovementMethod(new ScrollingMovementMethod());
        responseText.setMovementMethod(new ScrollingMovementMethod());
        apiUrlText.setMovementMethod(new ScrollingMovementMethod());

        apiUrlText.setText(apiUrl);
        requestText.setText(requestParams);
        responseText.setText(responseParams);
        Log.d(TAG, "requestType :" + requestType);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onResponseFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnResponseFragmentInteractionListener) {
            mListener = (OnResponseFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnTransactionProcessingFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnResponseFragmentInteractionListener {
        // TODO: Update argument type and name
        void onResponseFragmentInteraction(Uri uri);
    }
}
