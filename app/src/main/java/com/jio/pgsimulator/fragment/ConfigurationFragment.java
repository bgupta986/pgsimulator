package com.jio.pgsimulator.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.jio.pgsimulator.R;
import com.jio.pgsimulator.constant.UrlConstants;
import com.jio.pgsimulator.config.ApiConfiguration;
import com.jio.pgsimulator.util.CommonUtil;
import com.jio.pgsimulator.util.DateUtil;
import com.jio.pgsimulator.util.PreferenceManager;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnConfigurationFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ConfigurationFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ConfigurationFragment extends Fragment {

    private static final String TAG = ConfigurationFragment.class.getSimpleName();
    // TODO: Rename parameter arguments, choose names that match

    private OnConfigurationFragmentInteractionListener mListener;

    private View view;
    private TextInputLayout versionInput;
    private TextInputEditText timestampEditText, versionEditText, checksumSeedEditText, clientIdEditText, merchantIdEditText, returnUrlEditText, serviceUrlEditText;
    private Button saveButton;

    //    ProgressBar progressBar;
    public ConfigurationFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ConfigurationFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ConfigurationFragment newInstance() {
        ConfigurationFragment fragment = new ConfigurationFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_configuration, container, false);
        init();
        setConfigToUI();

        return view;
    }


    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "onStart()");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume()");
    }

    private void init() {
        versionInput = (TextInputLayout) view.findViewById(R.id.version_input);

        timestampEditText = (TextInputEditText) view.findViewById(R.id.timestamp_edit_text);
        versionEditText = (TextInputEditText) view.findViewById(R.id.version_edit_text);
        clientIdEditText = (TextInputEditText) view.findViewById(R.id.client_id_edit_text);
        merchantIdEditText = (TextInputEditText) view.findViewById(R.id.merchant_id_edit_text);
        checksumSeedEditText = (TextInputEditText) view.findViewById(R.id.checksum_seed_edit_text);
        returnUrlEditText = (TextInputEditText) view.findViewById(R.id.return_url_edit_txt);
        serviceUrlEditText = (TextInputEditText) view.findViewById(R.id.service_url_edit_text);
        saveButton = (Button) view.findViewById(R.id.save_button);

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonUtil.hideKeyboard(getActivity());
                saveConfig();
            }
        });
    }

    private void setConfigToUI() {
        ApiConfiguration apiConfiguration = PreferenceManager.getConfigPreference(getContext());
        if (null != apiConfiguration) {
            checksumSeedEditText.setText(apiConfiguration.getCHECKSUM_SEED());
            clientIdEditText.setText(apiConfiguration.getCLIENT_ID());
            merchantIdEditText.setText(apiConfiguration.getMERCHANT_ID());
            timestampEditText.setText(DateUtil.getCurrentDate());
            returnUrlEditText.setText(apiConfiguration.getReturnUrl());
            serviceUrlEditText.setText(apiConfiguration.getServiceUrl());
            versionEditText.setText(apiConfiguration.getVERSION());
        } else {
            returnUrlEditText.setText(UrlConstants.RETURN_URL);
            serviceUrlEditText.setText(UrlConstants.SERVICE_URL);
        }

    }

    private boolean isValidationPassed() {
        clearErrorLayout();
        if (versionEditText.getText().toString().length() > 0) {
            String version = versionEditText.getText().toString();
            if (version.equals("1.0") || version.equals("2.0")) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    private void clearErrorLayout() {
        versionInput.setErrorEnabled(false);
        versionInput.setError(null);
        versionEditText.clearFocus();
    }

    private void saveConfig() {
        if (isValidationPassed()) {
            ApiConfiguration apiConfiguration = new ApiConfiguration(versionEditText.getText().toString().trim(),
                    merchantIdEditText.getText().toString().trim(), clientIdEditText.getText().toString(),
                    checksumSeedEditText.getText().toString().trim(), returnUrlEditText.getText().toString().trim(),
                    serviceUrlEditText.getText().toString().trim());
            PreferenceManager.setConfigFreference(getContext(), apiConfiguration);

            Toast.makeText(getActivity(), "Configuration saved!", Toast.LENGTH_SHORT).show();
        }else {
            versionInput.setError("API Version not valid, Enter either 1.0 or 2.0!");
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onConfigurationFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnConfigurationFragmentInteractionListener) {
            mListener = (OnConfigurationFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnConfigurationFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnConfigurationFragmentInteractionListener {
        // TODO: Update argument type and name
        void onConfigurationFragmentInteraction(Uri uri);
    }
}
