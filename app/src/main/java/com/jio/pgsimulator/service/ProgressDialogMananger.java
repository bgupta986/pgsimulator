package com.jio.pgsimulator.service;

import android.app.ProgressDialog;
import android.content.Context;

/**
 * Created by COMPAQ on 26-04-2016.
 */
public class ProgressDialogMananger {
    private static ProgressDialog progressDialog;
    public void init(Context context){
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Processing");
        progressDialog.setCancelable(false);
    }

    public static ProgressDialog getInstance(Context context){
        if(null == progressDialog){
            return progressDialog;
        }
        return progressDialog;
    }

    public static void showProgress(Context context){
        if(null == progressDialog){
            getInstance(context);
        }
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Processing");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public static void hideProgressDialg(){
        if(null != progressDialog && progressDialog.isShowing()){
            progressDialog.dismiss();
            progressDialog = null;
        }
    }
}
