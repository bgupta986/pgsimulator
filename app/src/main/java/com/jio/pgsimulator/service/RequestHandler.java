package com.jio.pgsimulator.service;

/**
 * Created by Vishal.Chougule on 14-04-2016.
 */
public interface RequestHandler {

    public void onStart();
    public void onStop();
    public void onSuccess(int responseCode, String response);
    public void OnError(int responseCode, String response);
    public void OnNetworkConnectionError(int errorCode, String response);

}
