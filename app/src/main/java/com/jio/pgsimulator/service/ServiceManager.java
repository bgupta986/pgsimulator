package com.jio.pgsimulator.service;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;

import com.jio.pgsimulator.constant.RequestTypeArray;
import com.jio.pgsimulator.config.ApiConfiguration;
import com.jio.pgsimulator.util.PreferenceManager;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.X509TrustManager;

/**
 * Created by Vayana Developer on 14-04-2016.
 * The custom AsyncTask class is common for managing network calls
 */
public class ServiceManager extends AsyncTask<String, String, String> {

    private final String TAG = ServiceManager.class.getSimpleName();
    private Context context;
    private String requestParams = null;
    private com.jio.pgsimulator.service.RequestHandler requestHandler;
    private HttpMethod requestMethod = HttpMethod.GET;
    public static final int TIME_OUT = 1000;
    private String url;
    private int responseCode = 999;
    private String requestParamType = "";
    private static final int NETWORK_ERROR = 4;

    /**
     * Constructor to initialize class params
     * @param context Activity context on which the async task will run
     * @param url Web url for calling API
     * @param requestParams TransactionRequest params for particular API
     * @param method HTTP method for particular API
     * @param requestParamType Content type of particular API
     */
    public ServiceManager(Context context, String url, String requestParams, HttpMethod method, String requestParamType) {
        Log.d(TAG, "constructor");
        this.context = context;
        this.requestParams = requestParams;
        this.requestMethod = method;
        this.url = url;
        this.requestParamType = requestParamType;
    }

    /**
     * AsyncTask lifecycle method called for onstart method os request handler
     */
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        Log.d(TAG, "onPreExecute");
        requestHandler.onStart();
    }

    /**
     * AsyncTask lifecycle method do it's work in background
     * Does the network call throgh http urlconnection
     * @param params
     * @return
     */
    @Override
    protected String doInBackground(String... params) {

        ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService(context.CONNECTIVITY_SERVICE);
        NetworkInfo info = connectivityManager.getActiveNetworkInfo();
        if(!(null != info && info.isConnected())){
//            Toast.makeText(context, "Please check your network connection !!!", Toast.LENGTH_SHORT).show();
            responseCode = NETWORK_ERROR;
            String errorMsg = "Please check you internet connetion !!!";
            Log.e(TAG, errorMsg);
            return errorMsg;
        }
        String response = "";
        HttpsURLConnection urlConnection = null;

        try {
            /* Code for bypassing SSL*/
            /*SSLContext sslContext = SSLContext.getInstance("TLSv1");
            Log.d(TAG, "Url " + url);
            sslContext.init(null,
                    null,
                    null);
            SSLSocketFactory NoSSLv3Factory = new NoSSLv3Factory(sslContext.getSocketFactory());

            HttpsURLConnection.setDefaultSSLSocketFactory(NoSSLv3Factory);*/


            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier(){
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }});
            SSLContext context = SSLContext.getInstance("TLS");
            context.init(null, new X509TrustManager[]{new X509TrustManager(){
                public void checkClientTrusted(X509Certificate[] chain,
                                               String authType) throws CertificateException {}
                public void checkServerTrusted(X509Certificate[] chain,
                                               String authType) throws CertificateException {}
                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }}}, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(
                    context.getSocketFactory());


            URL uri = new URL(url);
            urlConnection = (HttpsURLConnection) uri.openConnection();
            if (requestMethod == HttpMethod.POST) {
                urlConnection.setDoOutput(true);
                urlConnection.setDoInput(true);
                urlConnection.setUseCaches(false);
                urlConnection.setConnectTimeout(TIME_OUT);
//                urlConnection.setRequestMethod(requestMethodArray[(int) HttpMethod.POST.name()]);
                Log.d(TAG, "HttpMethod " + HttpMethod.POST.name());
                urlConnection.setRequestMethod(HttpMethod.POST.name());
                ApiConfiguration apiConfig = PreferenceManager.getConfigPreference(this.context);
                urlConnection.setRequestProperty("APIVer", apiConfig.getVERSION());
                if(requestParamType.equalsIgnoreCase(RequestTypeArray.JSON.name())){
                    urlConnection.setRequestProperty("Content-Type", "application/json");
                    urlConnection.setRequestProperty("Accept", "application/json");
                }else if(requestParamType.equalsIgnoreCase(RequestTypeArray.XML.name())){
                    urlConnection.setRequestProperty("Content-Type", "application/xml");
                    urlConnection.setRequestProperty("Accept", "application/xml");
                }else{
                    Log.d(TAG, "application/x-www-form-urlencoded");
                    urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                }

                OutputStream os = urlConnection.getOutputStream();
                BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                bw.write(requestParams);
                bw.flush();
                bw.close();
                os.close();

                responseCode = urlConnection.getResponseCode();
                String newUrl = urlConnection.getHeaderField("Location");
                Log.d(TAG, "location " + newUrl);
                Log.d(TAG, "responseCode " + responseCode);
                if(responseCode == HttpURLConnection.HTTP_MOVED_TEMP){
                    urlConnection = (HttpsURLConnection)new URL(newUrl).openConnection();
                }

                String line;
                if (HttpURLConnection.HTTP_CREATED == responseCode || HttpURLConnection.HTTP_OK == responseCode) {
                    BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    while (null != (line = br.readLine())) {
                        response += line;
                    }
                } else {
                    BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getErrorStream()));
                    while (null != (line = br.readLine())) {
                        response += line;
                    }
                }
            }
        } catch (MalformedURLException e) {
            Log.d(TAG, "MalformedURLException " + e.getStackTrace());
            e.printStackTrace();
        } catch (IOException e) {
            Log.d(TAG, "IOException " + e.getStackTrace());
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } finally {
            if(null != urlConnection){
                urlConnection.disconnect();
            }
        }
        Log.d(TAG, "Response " + response);
        return response;
    }

    /**
     * AsyncTask lifecycle method executed after doInBackground finishes its work
     * @param response is value returned by doInBackground
     */
    @Override
    protected void onPostExecute(String response) {
        super.onPostExecute(response);
        requestHandler.onStop();
        if (HttpURLConnection.HTTP_CREATED == responseCode || HttpURLConnection.HTTP_OK == responseCode) {
            requestHandler.onSuccess(responseCode, response);
        } else if(responseCode == NETWORK_ERROR){
           requestHandler.OnNetworkConnectionError(responseCode, response);
        }else{
            requestHandler.OnError(responseCode, response);
        }
    }

    /**
     * For setting handler request before executing asysntask
     * @param requestHandler RequestHandler Object
     */
    public void handleRequest(com.jio.pgsimulator.service.RequestHandler requestHandler) {
        this.requestHandler = requestHandler;
    }


    /**
     * Enum constant defined for HTTP methods
     */
    public enum HttpMethod {
        GET,
        POST,
        PUT,
        DELETE
    }


}
