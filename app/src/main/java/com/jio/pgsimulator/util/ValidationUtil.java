package com.jio.pgsimulator.util;

import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.util.Log;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by COMPAQ on 20-04-2016.
 */
public class ValidationUtil {
    private final static String TAG = ValidationUtil.class.getSimpleName();

//    Setting inerror
    public static void setErrorLayout(TextInputLayout inputLayout, String errorMsg, TextInputEditText editText){
        Log.d(TAG, "setErrorLayout : inputLayout :" + inputLayout.toString());
        Log.d(TAG, "setErrorLayout : isEnabled :" + inputLayout.isErrorEnabled());

        if(null != inputLayout){
            Log.d(TAG, "inside if");
            Log.d(TAG, "setErrorLayout : errorMsg :" + errorMsg);
//            inputLayout.setErrorEnabled(true);
            inputLayout.setError(errorMsg);
            editText.requestFocus();

        }
    }

    // validating email id
    public static boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }
//    validation phone number
    public static boolean isValidMobile(String phone){
        return android.util.Patterns.PHONE.matcher(phone).matches();
    }
}
