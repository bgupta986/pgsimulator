package com.jio.pgsimulator.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.jio.pgsimulator.config.ApiConfiguration;

import java.io.Serializable;

/**
 * Created by COMPAQ on 21-04-2016.
 */
public class PreferenceManager implements Serializable{
    private final String TAG  = PreferenceManager.class.getSimpleName();
    private static SharedPreferences mPreference;
    private static SharedPreferences.Editor mEditor;

    private static final String CONFIG = "Config";

    private PreferenceManager(Context context){

    }

    private static SharedPreferences getPreference(Context context){
        if(null == mPreference){
            mPreference = context.getSharedPreferences(CONFIG, Context.MODE_PRIVATE);
        }
        return mPreference;
    }

    private static SharedPreferences getSharedPreferences(Context context){
        return getPreference(context);
    }

    private static SharedPreferences.Editor getEditor(Context context){
        if(null == mEditor){
            mEditor = getPreference(context).edit();
        }
        return mEditor;
    }

    public static ApiConfiguration getConfigPreference(Context context){
        String apiConfigData = getPreference(context).getString(ApiConfiguration.NAME, null);
        ApiConfiguration apiConfiguration = new Gson().fromJson(apiConfigData, ApiConfiguration.class);
        return apiConfiguration;
    }

    public static void setConfigFreference(Context context, ApiConfiguration apiConfiguration){
        getEditor(context).putString(ApiConfiguration.NAME, new Gson().toJson(apiConfiguration));
        getEditor(context).commit();
    }




}
