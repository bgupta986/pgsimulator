package com.jio.pgsimulator.util;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by COMPAQ on 19-04-2016.
 */
public class EncryptionUtil {

//    public static String CHECKSUM_SEED= "c762kj7q-9821";
    public static String ALGORITHM = "HmacSHA256";

    public String hmacDigest(String data, String secret, String algo) {
        String digest = null;
        try {
            SecretKeySpec key = new SecretKeySpec(secret.getBytes("UTF-8"), algo);
            Mac mac = Mac.getInstance(algo);
            mac.init(key);
            byte[] bytes = mac.doFinal(data.getBytes("UTF-8"));
            StringBuffer hash = new StringBuffer();
            for (int i = 0; i < bytes.length; i++) {
                String hex = Integer.toHexString(0xFF & bytes[i]);
                if (hex.length() == 1) {
                    hash.append('0');
                }
                hash.append(hex);
            }
            digest = hash.toString();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return digest;
    }
}
