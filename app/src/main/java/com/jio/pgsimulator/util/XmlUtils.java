package com.jio.pgsimulator.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jio.pgsimulator.model.OtherApiRequest;
import com.jio.pgsimulator.model.TranRequestProcessing;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;
import org.simpleframework.xml.strategy.VisitorStrategy;

import java.io.StringWriter;

/**
 * Created by vishal on 25/4/16.
 */
public class XmlUtils {

    public static String generateXmlParams(OtherApiRequest otherApi, String version) {
        StringWriter sw = new StringWriter();
        try {
            if (version.equals("2.0")) {
                Persister persister = new Persister(new VisitorStrategy(new OtherApiRequest.OtherApiSmallCaseXML()));
                persister.write(otherApi, sw);
            } else {
                Serializer serializer = new Persister();
                serializer.write(otherApi, sw);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sw.toString();
    }

    public static String generateXmlParams(TranRequestProcessing requestProcessing) {
        StringWriter sw = new StringWriter();
        try {
            Persister persister = new Persister();
            persister.write(requestProcessing, sw);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sw.toString();
    }
}