package com.jio.pgsimulator.util;

import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.jio.pgsimulator.model.TranRequest;
import com.jio.pgsimulator.model.OtherApiRequest;
import com.jio.pgsimulator.model.RefundRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.StringWriter;
import java.lang.reflect.Field;
import java.util.Iterator;

/**
 * Created by vishal on 24/4/16.
 */
public class CommonUtil {

    private static final String TAG  = CommonUtil.class.getSimpleName();

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static String getJsonString(RefundRequest refundRequest){
        StringWriter sw = new StringWriter();
        ObjectMapper objMapper = new ObjectMapper();
        try {
            objMapper.writeValue(sw, refundRequest);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        return sw.toString();
    }

    public static String getJsonString(OtherApiRequest otherApi){
        ObjectMapper mapper = new ObjectMapper();
        String str = null;
        try {
            mapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.NONE);
            mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
            ObjectWriter writer = mapper.writer();
            writer = writer.withDefaultPrettyPrinter();
            str = writer.writeValueAsString(otherApi);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        return str;
    }

    public static String getJsonString(TranRequest tranRequest){
        ObjectMapper mapper = new ObjectMapper();
        String str = null;
        try {
            mapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.NONE);
            mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
            ObjectWriter writer = mapper.writer();
            writer = writer.withDefaultPrettyPrinter();
            str = writer.writeValueAsString(tranRequest);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        return str;
    }

    private String getPostDataString(Object obj){
        if (obj.getClass().getDeclaredFields().length == 0)
            return "";

        StringBuffer buf = new StringBuffer();
        for (Field field : obj.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            buf.append(buf.length() == 0 ? "" : "&");
            try {
                if(field.get(obj) != null){
                    buf.append(field.getName()).append("=").append(field.get(obj));
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return buf.toString();
    }

    private String generateQueryStringFromJson(String jsonString){
        try{
            StringBuilder sb = new StringBuilder();
            JSONObject json = new JSONObject(jsonString);
            Iterator<String> keys = json.keys();
            //start of query argsi
            int i = 0;
            int len = json.length();
            Log.d(TAG, "len " + len);
            while (keys.hasNext()) {
                String key = keys.next();
                sb.append(key);
                sb.append("=");
                sb.append(json.get(key));
                //To allow for another argument.
                if(len-1 != i){
                    sb.append("&");
                }
                i++;
            }
            Log.d(TAG, "len " + i);
            return sb.toString();
        }catch (JSONException e){
            e.printStackTrace();
            return "";
        }
    }
}
