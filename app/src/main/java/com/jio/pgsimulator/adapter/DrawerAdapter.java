package com.jio.pgsimulator.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.jio.pgsimulator.R;

/**
 * Created by COMPAQ on 20-04-2016.
 */
public class DrawerAdapter extends BaseAdapter {
    private Context context;
    private String[] drawerList;
    LayoutInflater inflater;
    public DrawerAdapter(Context context, String[] drawerList){
        this.context = context;
        this.drawerList = drawerList;
        inflater = LayoutInflater.from(this.context);
    }

    @Override
    public int getCount() {
        return drawerList.length;
    }

    @Override
    public String getItem(int position) {
        return drawerList[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(null == convertView) {
            convertView = inflater.inflate(R.layout.drawer_item, parent, false);
            TextView titleText = (TextView)convertView.findViewById(R.id.item_title);
            titleText.setText(getItem(position));

        }
        return convertView;
    }
}
