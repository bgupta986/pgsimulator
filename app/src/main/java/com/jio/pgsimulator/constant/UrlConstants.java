package com.jio.pgsimulator.constant;

/**
 * Created by COMPAQ on 11-04-2016.
 */
public class UrlConstants {

    //https://testbill.rpay.co.in:8443/Services/TransactionInquiry
    //https://testpg.rpay.co.in/reliance-webpay/v1.0/payment/status
    private static final String TRANSACTION_INQUIRY_DOMAIN = "https://sitbill.rpay.co.in:8443/";
    private static final String PAYMENT_DOMAIN = "https://testpg.rpay.co.in/";

    public static final String RETURN_URL = "http://61.16.175.3:8089/radiocabweb/loadmoneyResult.jsp";
    public static final String SERVICE_URL = PAYMENT_DOMAIN + "reliance-webpay/v1.0/jiopayments";

    public static final String TRANSACTION_INQUIRY_URL = TRANSACTION_INQUIRY_DOMAIN + "Services/TransactionInquiry";
    public static final String STATUS_QUERY_URL = PAYMENT_DOMAIN + "/reliance-webpay/v1.0/payment/status";
    public static final String REFUND_URL = PAYMENT_DOMAIN + "reliance-webpay/jiorefund";
}
