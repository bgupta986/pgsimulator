package com.jio.pgsimulator.constant;

/**
 * Created by COMPAQ on 22-04-2016.
 */
public enum RequestTypeArray {
    JSON,
    XML;

    public static String[] names() {
        RequestTypeArray[] tpArray = values();
        String[] names = new String[tpArray.length];

        for (int i = 0; i < tpArray.length; i++) {
            names[i] = tpArray[i].name();
        }

        return names;
    }
}
