package com.jio.pgsimulator.constant;

/**
 * Created by COMPAQ on 20-04-2016.
 */
public enum OtherAPIArray {
    CHECK_PAYMENT_STATUS,
    GET_REQUEST_STATUS,
    GET_MDR,
    GET_TRANSACTION_DETAILS,
    FETCH_TRANSACTION_PERIOD,
    GET_TODAYS_DATA,
    STATUS_QUERY;

    public static String[] names() {
        OtherAPIArray[] tpArray = values();
        String[] names = new String[tpArray.length];

        for (int i = 0; i < tpArray.length; i++) {
            names[i] = tpArray[i].name();
        }

        return names;
    }
}
