package com.jio.pgsimulator.activity;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.jio.pgsimulator.R;
import com.jio.pgsimulator.adapter.DrawerAdapter;
import com.jio.pgsimulator.fragment.AboutUsFragment;
import com.jio.pgsimulator.fragment.ConfigurationFragment;
import com.jio.pgsimulator.fragment.OtherApiFragment;
import com.jio.pgsimulator.fragment.RefundFragment;
import com.jio.pgsimulator.fragment.ResponseFragment;
import com.jio.pgsimulator.fragment.TabLayoutFragment;


/**
 * The class is launcher activity illustrate application
 * flow includes navigation tab with configuration screen and simulator mode
 */
public class HomeActivity extends AppCompatActivity implements
        ConfigurationFragment.OnConfigurationFragmentInteractionListener,
        AboutUsFragment.OnAboutUsFragmentInteractionListener,
        TabLayoutFragment.OnTabLayoutFragmentInteractionListener,
        RefundFragment.OnRefundFragmentInteractionListener,
        OtherApiFragment.OnOtherApiFragmentInteractionListener,
        ResponseFragment.OnResponseFragmentInteractionListener {


    private final String TAG = HomeActivity.class.getSimpleName();
    Toolbar toolbar;

    private String[] drawerList;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private String requestType;
    private ActionBarDrawerToggle mDrawerToggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        initComponent();
        selectItem(0);
    }

    /**
     * This method initializes all the components
     */
    private void initComponent() {
        drawerList = getResources().getStringArray(R.array.drawer_items);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);

//        ArrayList<String> arraylist = (ArrayList<String>)Arrays.asList(drawerList);

        // Set the adapter for the list view
        mDrawerList.setAdapter(new DrawerAdapter(this, drawerList));
//        Collections.addAll(drawerList);
        // Set the list's click listener
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar,
                R.string.app_name, R.string.app_name) {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                //getActionBar().setTitle("XYZ");
//                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
//                getActionBar().setTitle("XYZ");
//                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
                mDrawerList.bringToFront();
                mDrawerLayout.requestLayout();
            }
        };


        // Set the drawer toggle as the DrawerListener
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        setSupportActionBar(toolbar);
        mDrawerToggle.syncState();
    }


    /**
     * This methods calls when aapli
     */
    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
    }

    @Override
    protected void onStop() {
        super.onStop();
    }


    /**
     * This methid us used to set request type i.e JSON or XML
     *
     * @param requestType to set value of request type
     */
    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    /**
     * This method is used to get selected request type
     *
     * @return current request type
     */

    public String getRequestType() {
        return requestType;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle your other action bar items...

        return super.onOptionsItemSelected(item);
    }

    /**
     * Swaps fragments in the main content view
     */
    private void selectItem(int position) {
        // Create a new fragment and specify the planet to show based on position
        Log.d(TAG, "selectItem position :" + position);
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = TabLayoutFragment.newInstance();
                break;
            case 1:
                fragment = ConfigurationFragment.newInstance();
                break;
            case 2:
                fragment = AboutUsFragment.newInstance();
                break;
        }

        // Insert the fragment by replacing any existing fragment
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.content_frame, fragment)
                .commit();

        // Highlight the selected item, update the title, and close the drawer
        mDrawerList.setItemChecked(position, true);
        setTitle(drawerList[position]);
        mDrawerLayout.closeDrawer(mDrawerList);
    }

    private void showResponseFragment(String req, String res, String apiUrl) {
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        mDrawerToggle.setDrawerIndicatorEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Fragment fragment = ResponseFragment.newInstance(req, res, apiUrl, getRequestType());
        FragmentTransaction fm = getSupportFragmentManager().beginTransaction();
        fm.replace(R.id.content_frame, fragment).addToBackStack(ResponseFragment.TAG).commit();
        ;
    }

    @Override
    public void setTitle(CharSequence title) {
        toolbar.setTitle(title);
    }

    @Override
    public void onConfigurationFragmentInteraction(Uri uri) {

    }

    @Override
    public void onTabLayoutFragmentInteraction(Uri uri) {

    }

    @Override
    public void onResponseFragmentInteraction(Uri uri) {
    }

    @Override
    public void onBackPressed() {

        if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
            mDrawerToggle.setDrawerIndicatorEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } else {
//            getSupportFragmentManager().popBackStack();
        }
        super.onBackPressed();
    }

    @Override
    public void onOtherApiFragmentInteraction(String request, String response, String apiUrl) {
        showResponseFragment(request, response, apiUrl);
    }

    @Override
    public void onRefundFragmentInteraction(String req, String resp, String apiUrl) {
        showResponseFragment(req, resp, apiUrl);

    }

    @Override
    public void onAboutUsFragmentInteraction(Uri uri) {

    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView parent, View view, int position, long id) {
            Log.d(TAG, "onItemClick called position: " + position);
            selectItem(position);
        }
    }


}
