package com.jio.pgsimulator.config;

/**
 * Created by COMPAQ on 21-04-2016.
 */
public class ApiConfiguration {

    public static final String NAME = ApiConfiguration.class.getSimpleName();

    private String VERSION;
    private String MERCHANT_ID;
    private String CLIENT_ID;
    private String CHECKSUM_SEED;
    private String returnUrl;
    private String serviceUrl;

    public ApiConfiguration(String VERSION, String MERCHANT_ID, String CLIENT_ID, String CHECKSUM_SEED, String returnUrl, String serviceUrl) {
        this.VERSION = VERSION;
        this.MERCHANT_ID = MERCHANT_ID;
        this.CLIENT_ID = CLIENT_ID;
        this.CHECKSUM_SEED = CHECKSUM_SEED;
        this.returnUrl = returnUrl;
        this.serviceUrl = serviceUrl;
    }

    public String getReturnUrl() {
        return returnUrl;
    }

    public String getServiceUrl() {
        return serviceUrl;
    }

    public static String getNAME() {

        return NAME;
    }

    public String getVERSION() {
        return VERSION;
    }

    public String getMERCHANT_ID() {
        return MERCHANT_ID;
    }

    public String getCLIENT_ID() {
        return CLIENT_ID;
    }

    public String getCHECKSUM_SEED() {
        return CHECKSUM_SEED;
    }
}
