package com.jio.pgsimulator.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Created by COMPAQ on 07-06-2016.
 */
@JsonPropertyOrder({"amount","currency", "extref", "timestamp", "txntype"})
public class RefundTransaction {

    private String amount;
    private String currency;
    private String extref;
    private String txntype;
    private String timestamp;

    public RefundTransaction(String amount, String currency, String extref, String txntype, String timestamp) {
        this.amount = amount;
        this.currency = currency;
        this.extref = extref;
        this.txntype = txntype;
        this.timestamp = timestamp;
    }

    @JsonProperty("amount")
    public String getAmount() {
        return amount;
    }

    @JsonProperty("currency")
    public String getCurrency() {
        return currency;
    }

    @JsonProperty("extref")
    public String getExtref() {
        return extref;
    }

    @JsonProperty("txntype")
    public String getTxntype() {
        return txntype;
    }

    @JsonProperty("timestamp")
    public String getTimestamp() {
        return timestamp;
    }
}
