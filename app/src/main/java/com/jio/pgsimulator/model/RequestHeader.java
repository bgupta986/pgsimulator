package com.jio.pgsimulator.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Order;
import org.simpleframework.xml.Root;

/**
 * Created by COMPAQ on 15-04-2016.
 */
@Root
@Order(elements = {"api_name", "timestamp", "version"})
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({"api_name", "timestamp", "version"})
public class RequestHeader {
    @Element(name = "api_name", required = false)
    private String api_name;

    @Element(name = "timestamp", required = false)
    private String timestamp;

    @Element(name = "version", required = false)
    private String version;

    public RequestHeader(String VERSION, String API_NAME, String s) {
        this.api_name = API_NAME;
        this.version = VERSION;
    }

    //For CHECK PAYMENT STATUS API
    public RequestHeader(String API_NAME, String timestamp) {
        this.api_name = API_NAME;
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "RequestHeader{" +
                "api_name='" + api_name + '\'' +
                ", version='" + version + '\'' +
                '}';
    }
}
