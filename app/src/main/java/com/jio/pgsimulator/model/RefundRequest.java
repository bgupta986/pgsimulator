package com.jio.pgsimulator.model;

/**
 * Created by COMPAQ on 07-06-2016.
 */
public class RefundRequest {

    private String merchantid;
    private String clientid;
    private String channel;
    private String returl;
    private String token;
    private String checksum;
    private String refundinfo;
    private RefundTransaction transaction;

    public RefundRequest(String merchantid, String clientid, String channel, String returl,
                         String token, String checksum, String refundinfo, RefundTransaction transaction) {
        this.merchantid = merchantid;
        this.clientid = clientid;
        this.channel = channel;
        this.returl = returl;
        this.token = token;
        this.checksum = checksum;
        this.refundinfo = refundinfo;
        this.transaction = transaction;
    }

    public String getMerchantid() {
        return merchantid;
    }

    public String getClientid() {
        return clientid;
    }

    public String getChannel() {
        return channel;
    }

    public String getReturl() {
        return returl;
    }

    public String getToken() {
        return token;
    }

    public String getChecksum() {
        return checksum;
    }

    public String getRefundinfo() {
        return refundinfo;
    }

    public RefundTransaction getTransaction() {
        return transaction;
    }
}
