package com.jio.pgsimulator.model;

/**
 * Created by COMPAQ on 15-04-2016.
 */
public class TranRequest {

    TranRequestProcessing request;

    public TranRequest(TranRequestProcessing request) {
        this.request = request;
    }

    @Override
    public String toString() {
        return "TranRequestProcessing{" +
                "TranRequestProcessing=" + request +
                '}';
    }
}
