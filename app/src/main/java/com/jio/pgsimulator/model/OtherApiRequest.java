package com.jio.pgsimulator.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Order;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.strategy.Type;
import org.simpleframework.xml.strategy.Visitor;
import org.simpleframework.xml.stream.InputNode;
import org.simpleframework.xml.stream.NodeMap;
import org.simpleframework.xml.stream.OutputNode;

/**
 * Created by COMPAQ on 20-04-2016.
 */
@Root(name = "REQUEST")
@Order(elements={"REQUEST_HEADER", "PAYLOAD_DATA", "CHECKSUM"})
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({"request_header", "payload_data", "checksum"})
public class OtherApiRequest {
    @Element(name = "REQUEST_HEADER")
    RequestHeader request_header;

    @Element(name = "PAYLOAD_DATA")
    OtherApiPayloadData payload_data;

    @Element(name = "CHECKSUM")
    String checksum;

    public OtherApiRequest(RequestHeader REQUEST_HEADER, OtherApiPayloadData PAYLOAD_DATA, String CHECKSUM) {
        this.request_header = REQUEST_HEADER;
        this.payload_data = PAYLOAD_DATA;
        this.checksum = CHECKSUM;
    }

    @Override
    public String toString() {
        return "TransactionProcessing{" +
                "payload_data=" + payload_data +
                '}';
    }

    public static class OtherApiSmallCaseXML implements Visitor {
        @Override
        public void read(Type type, NodeMap<InputNode> nodeMap) throws Exception {
        }

        @Override
        public void write(Type type, NodeMap<OutputNode> nodeMap) throws Exception {
            //Log.d(TAG, "inside write type :"+type);
            OutputNode outputNode = nodeMap.getNode();
            //Log.d(TAG, "inside write name :"+outputNode.getName());
            outputNode.setName(outputNode.getName().toLowerCase());
        }
    }
}