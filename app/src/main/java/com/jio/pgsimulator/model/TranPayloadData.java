package com.jio.pgsimulator.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementArray;
import org.simpleframework.xml.Order;
import org.simpleframework.xml.Root;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by COMPAQ on 20-04-2016.
 */

@Root
@Order(elements = {"mid", "tran_details", "startdatetime", "enddatetime"})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TranPayloadData {

    @Element(name = "mid", required = false)
    private String mid;

//    @ElementArray(name = "tran_details", required = false)
//    private List<TranRefNoArray> tran_details;

    @Element(name = "tran_details", required = false)
    private TranRefNoArray tran_details;

    @Element(name = "startdatetime", required = false)
    private String startdatetime;

    @Element(name = "enddatetime", required = false)
    private String enddatetime;

    public TranPayloadData(String mid) {
        this.mid = mid;
    }

//    public TranPayloadData(String mid, List<TranRefNoArray> tran_details) {
//        this.mid = mid;
//        this.tran_details = tran_details;
//    }

    public TranPayloadData(String mid, TranRefNoArray tran_details) {
        this.mid = mid;
        this.tran_details = tran_details;
    }

    public TranPayloadData(String mid, String startdatetime, String enddatetime) {
        this.mid = mid;
        this.tran_details = null;
        this.startdatetime = startdatetime;
        this.enddatetime = enddatetime;
    }
}