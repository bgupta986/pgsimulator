package com.jio.pgsimulator.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Order;
import org.simpleframework.xml.Root;

/**
 * Created by COMPAQ on 20-04-2016.
 */
@Root
@Order(elements={"CLIENT_ID", "MERCHANT_ID", "TRAN_REF_NO"})
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({"client_id", "merchant_id", "tran_ref_no"})
public class OtherApiPayloadData {
    @Element(name = "MERCHANT_ID", required = false)
    private String merchant_id;

    @Element(name = "TRAN_REF_NO", required = false)
    private String tran_ref_no;

    @Element(name = "CLIENT_ID", required = false)
    private String client_id;

    public OtherApiPayloadData(String MERCHANT_ID, String TRAN_REF_NO, String CLIENT_ID) {
        this.merchant_id = MERCHANT_ID;
        this.tran_ref_no = TRAN_REF_NO;
        this.client_id = CLIENT_ID;
    }
}
