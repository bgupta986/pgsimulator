package com.jio.pgsimulator.model;

import android.util.Log;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Order;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.strategy.Type;
import org.simpleframework.xml.strategy.Visitor;
import org.simpleframework.xml.stream.InputNode;
import org.simpleframework.xml.stream.NodeMap;
import org.simpleframework.xml.stream.OutputNode;

/**
 * Created by COMPAQ on 20-04-2016.
 */
@Root(name = "request")
@Order(elements = {"request_header", "payload_data", "checksum"})
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({"request_header", "payload_data", "checksum"})
public class TranRequestProcessing {

    public static final String TAG = TranRequestProcessing.class.getSimpleName();

    @Element(name = "request_header")
    RequestHeader request_header;

    @Element(name = "payload_data")
    TranPayloadData payload_data;

    @Element(name = "checksum")
    String checksum;

    public TranRequestProcessing(RequestHeader request_header, TranPayloadData payload_data, String checksum) {
        this.request_header = request_header;
        this.payload_data = payload_data;
        this.checksum = checksum;
    }

    @Override
    public String toString() {
        return "RefundProcessing{" +
                "payload_data=" + payload_data +
                '}';
    }

    public static class RefundProcessingSmallCaseXML implements Visitor {

        @Override
        public void read(Type type, NodeMap<InputNode> nodeMap) throws Exception {

        }

        @Override
        public void write(Type type, NodeMap<OutputNode> nodeMap) throws Exception {
            Log.d(TAG, "inside write type :" + type);
            OutputNode outputNode = nodeMap.getNode();
            Log.d(TAG, "inside write name :" + outputNode.getName());
            outputNode.setName(outputNode.getName().toLowerCase());
        }
    }
}
