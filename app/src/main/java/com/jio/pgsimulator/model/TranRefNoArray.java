package com.jio.pgsimulator.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementArray;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Order;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * Created by COMPAQ on 20-04-2016.
 */

@Root
@Order(elements = {"tran_ref_no"})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TranRefNoArray {

//    @ElementArray(name = "tran_ref_no")
//    private List<String> tran_ref_no;
//
//    public TranRefNoArray(List<String> tran_ref_no) {
//        this.tran_ref_no = tran_ref_no;
//    }

    @ElementList(name = "tran_ref_no")
    private String[] tran_ref_no;

    public TranRefNoArray(String[] tran_ref_no) {
        this.tran_ref_no = tran_ref_no;
    }

//    @Element(name = "tran_ref_no", required = false)
//    private  String tran_ref_no;
//    public TranRefNoArray(){
//
//    }
//    public TranRefNoArray(String tran_ref_no) {
//        this.tran_ref_no = tran_ref_no;
//    }
}